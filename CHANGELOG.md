
## 0.0.5 [04-15-2020]

* Update IAPDependencies and remove app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/command-template-best-practices!8

---

## 0.0.4 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/command-template-best-practices!6

---

## 0.0.3 [03-04-2020]

* Merged latest code with master

See merge request itentialopensource/pre-built-automations/staging/command-template-best-practices!2

---

## 0.0.2 [03-04-2020]

* Merged latest code with master

See merge request itentialopensource/pre-built-automations/staging/command-template-best-practices!2

---
