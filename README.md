


# Command-Template-Best-Practices

## Table of Contents
* [Overview](#overview)
* [Features](#features)
* [Requirements](#requirements)
* [Known Limitations](#known-limitations)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Components](#components)
	* [Workflows](#workflows)
	* [Command Templates](#command-templates)
	* [Automation Catalog Job](#automation-catalog-job)
* [Job Variables](#job-variables)
* [Test Environment](#test-environment)
* [Additional Information](#additional-information)

## Overview
The Command Template Best Practices artifact showcase the following features of command templates: 

 1. The ability to skip over commands if certain conditions are met
 2. Extract value(s) from the output of a command run that can be used as variables or arrays in other commands
 3. Running a command against variables in an array, looping through each variable until all values in the array have been used​

### Main Workflow
Pictured below is the main workflow for this artifact:
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/workflow_demo.png" alt="demo flow" width="800px">
</td></tr></table>

This workflow serves as an entry point for automation catalog to run the 3 use cases described above.


## Features
* Includes an example to show the ability to skip over commands if certain conditions are met
* Includes an example to show  extraction of value(s) from the output of a command run to be used as variables or arrays in other commands
* Includes an example to show running a command against variables in an array, looping through each variable until all values in the array have been used​
* Modular Design
* User customizable memory percentage array for use case 3

## Requirements
In order to use the artifact, users will have to satisfy one of the following pre-requisites:
* Itential Automation Platform `^2019.2.7` and App-Artifacts `^2.6.2`
* Itential Automation Platform `^2019.3.1` and App-Artifacts `^3.0.1`

## Known Limitations
At the time of this writing, the form does not make it mandatory for the user to input an array when the user chooses to run use case 3.

## How to Install
Please ensure that you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [requirements](#requirements) section in order to install the Command Template Best Practices artifact. If you do not currently have App-Artifacts installed on your server, please download the installer from your Nexus repository. Please refer to the instructions included in the App-Artifacts README to install it.
The Command Template Best Practices artifact can be installed from within App-Artifacts. Simply search for `command-template-best-practices` and click the install button as shown below:

<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/install.png" alt="install" width="600px">
</td></tr></table>
Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in App-Artifacts. Please consult the documentation for App-Artifacts for further information.

## How to Run
### Automation Catalog
The artifact can run from the Automation Catalog. First, select the use case you want to run. Next, select the devices you want to run the commands on. Finally, if you choose to run use case 3, click on the `+` icon under `Number Array` and add integers between 0 - 100 for the memory percentage check.
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/form_items.png" alt="form item" width="600px">
</td></tr></table>

## Components
This artifact is comprised of a set of modular components intended to modularize and simplify the features of Command Templates. 

### Workflows
#### Use Case 1 : Skip Command flow
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/workflow_uc1.png" alt="skip command flow" width="800px">
</td></tr></table>
This workflow is designed to run the following set of commands sequentially based on the scenario described below:

1. `show cpu` – if CPU utilization is > 90% skip to step 5​
2. `show memory` – if memory utilization is > 70% skip to step 4​
3. `show interface`​
4. `show ip ospf`
5. `show ip ospf neighbor`

Step 1 was checked using the [check cpu](#check-cpu) command template and Step 2 was checked using the [check memory static](#check-memory-static) command template, both of which have been described in detail further ahead.

#### Use Case 2 : Extract Command Output flow
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/workflow_uc2.png" alt="extract command output flow" width="800px">
</td></tr></table>
This workflow is designed to run two sets of commands sequentially - by using a value extracted from the first command in the second command, as described below:

1. `show cpu `– extract the value of CPU utilization
2. `show memory` – check if memory utilization is > CPU utilization from step 1​

Step 1 was checked using the [check cpu](#check-cpu) command template and Step 2 was checked using the [check memory dynamic](#check-memory-dynamic) command template, both of which have been described in detail further ahead.

#### Use Case 3 : Loop Command Variables flow
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/workflow_uc3.png" alt="loop command variables flow" width="600px">
</td></tr></table>
This workflow is designed to run one command multiple times - once for every value in the input array, as described below:

`show memory` – check if memory utilization is < every value in the dynamically input percentage array, for example: `[100, 90, 80, 70, 60, 50]`

The command was checked using the [check memory dynamic](#check-memory-dynamic) command template, which is described in detail further ahead.

### Command Templates
#### 1) Check Cpu
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/mop_uc1.1.png" alt="check cpu" width="400px">
</td></tr></table>

This command template runs the `show cpu` command and checks the output against a statically defined percentage number.

#### 2) Check Memory Static
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/mop_uc1.2.png" alt="check memory static" width="400px">
</td></tr></table>

This command template runs the `show memory` command and checks the output against a statically defined percentage number. 

#### 3) Check Memory Dynamic
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/mop_uc2.0.png" alt="check memory dynamic" width="400px">
</td></tr></table>

This command template runs the `show memory` command and checks the output against a percentage number passed in as a variable by the workflow job where this template is called. The variable has to be an object with the key as `memory_percentage` and an integer as the value.

### Automation Catalog Job
<table><tr><td>
 <img src="https://gitlab.com/itentialopensource/pre-built-automations/command-template-best-practices/raw/master/images/ac_job.png" alt="automation" width="600px">
</td></tr></table>

This artifact can be run by using automation catalog. Please check [How to Run](#how-to-run) for instructions on filling out the form.

## Job Variables
Should you decide to run the artifact without using automation catalog, the following job variables are required to start the artifact.

### [All Workflows](#workflows) 

<table>
<thead>
<tr>
<th>Job Variable</th>
<th>Type</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>device</code></td>
<td>string/array</td>
<td>Device(s) for the commands to be run on</td>
</tr>
<tr>
<td><code>number_array</code><br>(only for use case 3)</td>
<td>array<br>(only integers between 0-100)</td>
<td>Threshold(s) for the memory utilization percentage</td>
</tr>
</tbody>
</table>

### [Check Memory Command Template](#user-content-3-check-memory-dynamic) 

<table>
<thead>
<tr>
<th>Variable</th>
<th>Type</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>memory_percentage</code></td>
<td>integer</td>
<td>Threshold for the memory utilization percentage</td>
</tr>
</tbody>
</table>

## Test Environment
* IAP version 2019.2.7
* IAP version 2019.3.1

## Additional Information
Please use your Itential Customer Success account if you need support when using this artifact. 
